const request = require('supertest');
const app = require('../src/index');

describe('GET /', function() {
    it('displays "Hello Home Page!"', function (done) {
        request(app)
            .get('/')
            .expect('Hello Home Page!', done);
    });
});