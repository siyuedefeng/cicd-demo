const http = require('http');
const home = require('./home');

const server = http.createServer((request, response) => {
    if (request.url === '/') {
        return home(response);
    }
    response.end('Not Found');
});

const app = server.listen(3000, function() {
    console.log('server is listening at 3000 port.');
});

console.log(server.address, app.address);

module.exports = app;